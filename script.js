$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        dots: false,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 2,
                nav: true
            },
            1000: {
                items: 3,
                nav: false,
            }
        }
    });

    let phone = document.querySelector('#get-phone');

    phone.addEventListener('input', function (e) {
        phone.value = e.target.value.replace(/\D/g, '');
    });

    const tooltipWrappers = document.querySelectorAll('.tooltip-wrapper');

    for (let i = 0; i < tooltipWrappers.length; i++) {
        let tooltip = tooltipWrappers[i].querySelector('.tooltip');
        let control = tooltipWrappers[i].querySelector('.form-control');

        control.addEventListener('focus', function () {
            tooltip.style.display = 'block';
        });

        control.addEventListener('blur', function () {
            tooltip.style.display = 'none';
        });
    }

    let minutes = document.querySelector('.timer__min');
    let seconds = document.querySelector('.timer__sec');
    let timeInSec = 30 * 60;

    minutes.innerHTML = timeInSec / 60 + ' мин.'
    seconds.innerHTML = 0 + ' сек.';

    let step = 1;

    let timerID = setInterval(() => {
        timeInSec = timeInSec - step;

        let min = Math.floor(timeInSec / 60);
        let sec = timeInSec - min * 60;

        if (timeInSec === 0) {
            clearInterval(timerID);
        }

        minutes.innerHTML = min + ' мин.';
        seconds.innerHTML = sec + ' сек.'
    }, 1000);
});
